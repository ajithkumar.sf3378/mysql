-- database
create database mysqlassignment;

-- table pieces
create table Pieces(
	code int,
    name varchar(50),
    primary key (code)
);

-- table Provides
create table Provides (
	Piece int,
    Provider varchar(50),
    Price int
    );
    
-- table Providers
create table Providers (
	code varchar(50),
    name varchar(50)
    
    );
   
-- table pieces insert
insert into pieces (code ,name)
values(1,'Sprocket');
insert into pieces (code ,name)
values(2,'Screw');
insert into pieces (code ,name)
values(3,'Nut');
insert into pieces (code ,name)
values(4,'Bolt');

-- table Provides insert
insert into Provides  (Piece, Provider, Price)
values(1,'HAL',10);
insert into Provides  (Piece, Provider, Price)
values(1,'RBT',15);
insert into Provides  (Piece, Provider, Price)
values(2,'HAL',20);
insert into Provides  (Piece, Provider, Price)
values(2,'RBT',15);
insert into Provides  (Piece, Provider, Price)
values(2,'TNBC',14);
insert into Provides  (Piece, Provider, Price)
values(3,'RBT',50);
insert into Provides  (Piece, Provider, Price)
values(3,'TNBC',45);
insert into Provides  (Piece, Provider, Price)
values(4,'HAL',5);
insert into Provides  (Piece, Provider, Price)
values(4,'RBT',7);

-- table Providers insert
insert into Providers (code, name)
values('HAL','Clarke Enterprises');
insert into Providers (code, name)
values('RBT','Susan Calvin Corp');
insert into Providers (code, name)
values('TNBC','Skellington Supplies');

-- a1_e1
select name from pieces;

-- a1_e2
select * from providers;

-- a1_e3
SELECT Piece,avg(Price) FROM mysqlassignment.provides;

-- a1_e4
SELECT Provider FROM mysqlassignment.provides where Piece=1;




-- a1_e5
select name from Providers where code in( 
select Provider from Provides where provider='HAL'
);

-- a1_e6
SELECT * FROM mysqlassignment.provides order by price desc limit 2;


-- a1_e8
select Price,price+1 as priceonecent from Provides ;


-- a2_e2
SELECT ProductID,ProductName FROM northwind.products where discontinued='y' order by ProductName;

-- a2_e3
SELECT ProductName as ten_most_expensive_products,UnitPrice
 FROM northwind.products order by UnitPrice desc limit 10;
 
 -- a2_e4
 SELECT c.CategoryName,ProductName,QuantityPerUnit,UnitsInStock,Discontinued 
FROM northwind.products 
left join categories c
using(CategoryID)
where Discontinued ='n'
order by c.CategoryName;
 
 
 